﻿using System.Web.Mvc;
using Autofac;
using Autofac.Integration.Mvc;

namespace Request.Web
{
    public static class ContainerConfig
    {
        public static void SetResolver()
        {
            var builder = new ContainerBuilder();

            builder.RegisterControllers(typeof(MvcApplication).Assembly);

            var currentAssemby = typeof(ContainerConfig).Assembly;

            var otherAssemblies = new[]
            {
                Data.DataAssembly.Identity,
                Service.ServiceAssembly.Identity
            };

            builder.RegisterAssemblyModules(currentAssemby);
            builder.RegisterAssemblyModules(otherAssemblies);

            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
    }
}