﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Request.Web.Startup))]
namespace Request.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
