﻿using System.Web;
using Request.Contract;

namespace Request.Web.Authorization
{
    public static class HttpContextExtensions
    {
        public static RequestIdentity GetOfficeParkingIdentity(this HttpContext context)
        {
            return context?.User?.Identity as RequestIdentity;
        }
    }

    public static class HttpContextBaseExtensions
    {
        public static RequestIdentity GetOfficeParkingIdentity(this HttpContextBase context)
        {
            return context?.User?.Identity as RequestIdentity;
        }
    }
}