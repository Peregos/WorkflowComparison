﻿namespace Request.Web.Models
{
    public class FilterButton
    {
        public string Id { get; set; }
        public string Href { get; set; }
        public string Label { get; set; }
        public bool IsActive { get; set; }
    }
}