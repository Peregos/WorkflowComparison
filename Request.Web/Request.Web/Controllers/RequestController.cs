﻿using System.Web.Mvc;

namespace Request.Web.Controllers
{
    public class RequestController : Controller
    {
        // GET: Request
        public ActionResult CreateRequest()
        {
            return View(new Contract.Request());
        }
    }
}