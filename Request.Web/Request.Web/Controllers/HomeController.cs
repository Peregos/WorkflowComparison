﻿using System.Linq;
using System.Web.Mvc;
using Request.Contract;
using Request.Contract.Services;
using Request.Web.Authorization;

namespace Request.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly IRequestService _requestService;
        private User _currentUser;

        //public User CurrentEmployee => _currentUser ?? (_currentUser = ControllerContext.HttpContext.GetOfficeParkingIdentity().User);

        public HomeController(IRequestService requestService)
        {
            _requestService = requestService;
        }

        public ActionResult Index()
        {
            var requests = _requestService.GetRequestsByUserid(1);
            return View(requests);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}