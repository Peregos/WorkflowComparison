﻿using Autofac;
using Autofac.Extras.DynamicProxy;
using Request.Contract.Services;

namespace Request.Service
{
    public class Registrations : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterAssemblyTypes(ThisAssembly)
                .AsClosedTypesOf(typeof(IMapper<,>))
                .AsImplementedInterfaces();

            builder.RegisterType<RequestService>()
                .As<IRequestService>().EnableInterfaceInterceptors();

            builder.RegisterType<UserService>()
                .As<IUserService>().EnableInterfaceInterceptors();
        }
    }
}
