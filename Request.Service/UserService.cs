﻿using System.Linq;
using Request.Contract;
using Request.Contract.Services;
using Request.Data.Context;

namespace Request.Service
{
    public class UserService : IUserService
    {
        private readonly IRequestDbContext _dbContext;
        private readonly IMapper<Data.Entities.User, User> _dataToEntityMapper;
        private readonly IMapper<User, Data.Entities.User> _entityToDataMapper;

        public UserService(IRequestDbContext dbContext, IMapper<Data.Entities.User, User> dataToEntityMapper, IMapper<User, Data.Entities.User> entityToDataMapper)
        {
            _dbContext = dbContext;
            _dataToEntityMapper = dataToEntityMapper;
            _entityToDataMapper = entityToDataMapper;
        }

        public User GetUserById(int userId)
        {
            var user = _dbContext.Users.SingleOrDefault(u => u.Id == userId);
            return _dataToEntityMapper.Map(user);
        }

        public int AddUser(User user)
        {
            var userData = _entityToDataMapper.Map(user);
            _dbContext.Users.Add(userData);
            return _dbContext.SaveChanges();
        } 
    }
}
