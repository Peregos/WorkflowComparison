﻿using System;
using Request.Contract;
using Request.Contract.Services;

namespace Request.Service.Mapper
{
    public class RequestMapper :
        IMapper<Data.Entities.Request, Contract.Request>,
        IMapper<Contract.Request, Data.Entities.Request>
    {
        private IMapper<Data.Entities.User, User> _dataToEntityMapper;
        private IMapper<User, Data.Entities.User> _entityToDataMapper;

        public RequestMapper(
            IMapper<Data.Entities.User, User> dataMapper,
            IMapper<User, Data.Entities.User> entityMapper)
        {
            _dataToEntityMapper = dataMapper;
            _entityToDataMapper = entityMapper;
        }

        public Data.Entities.Request Map(Contract.Request from)
        {
            var request = new Data.Entities.Request
            {
                Id = from.Id,
                Status = from.Status.ToString(),
                TypeId = from.Type?.Id ?? -1,
                AssistantId = from.Assistant?.Id ?? -1,
                RequestorId = from.Requestor?.Id ?? -1,
                DateOfCreation = from.DateOfCreation
            };
            return request;
        }

        public Contract.Request Map(Data.Entities.Request @from)
        {
            var request = new Contract.Request
            {
                Id = from.Id,
                Status = (RequestStatus) Enum.Parse(typeof(RequestStatus),from.Status),
                Type = from.Type != null ? new RequestType {Id=from.TypeId, Name = from.Type.Name} : null,
                Assistant = from.Assistant != null ? _dataToEntityMapper.Map(from.Assistant) : null,
                Requestor = from.Requestor!=null? _dataToEntityMapper.Map(from.Requestor) : null,
                DateOfCreation = from.DateOfCreation
            };
            return request;
        }
    }
}
