﻿using System.Linq;
using Request.Contract;
using Request.Contract.Services;

namespace Request.Service.Mapper
{
    public class UserMapper :
        IMapper<Data.Entities.User, User>,
        IMapper<User, Data.Entities.User>
    {
        public User Map(Data.Entities.User @from)
        {
            var user = new User
            {
                Id = from.Id,
                Name = from.Name,
                Roles = from.UserGroups.SelectMany(g=>g.Roles).Select(r=>(Role)r.Id).Distinct().ToList()
            };
            return user;
        }

        public Data.Entities.User Map(User @from)
        {
            var user = new Data.Entities.User
            {
                Id = from.Id,
                Name = from.Name
            };
            return user;
        }
    }
}
