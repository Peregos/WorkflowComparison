﻿using Request.Data.Context;
using System.Collections.Generic;
using System.Linq;
using Request.Contract.Services;

namespace Request.Service
{
    public class RequestService : IRequestService
    {
        private readonly IRequestDbContext _dbContext;
        private readonly IMapper<Data.Entities.Request, Contract.Request> _dataToEntityMapper;
        private readonly IMapper<Contract.Request, Data.Entities.Request> _entityToDataMapper;

        public RequestService(IRequestDbContext dbContext, IMapper<Data.Entities.Request, Contract.Request> dataToEntityMapper, IMapper<Contract.Request, Data.Entities.Request> entityToDataMapper)
        {
            _dbContext = dbContext;
            _dataToEntityMapper = dataToEntityMapper;
            _entityToDataMapper = entityToDataMapper;
        }

        public Contract.Request GetRequestById(int requestId)
        {
            var request = _dbContext.Requests.SingleOrDefault(r => r.Id == requestId);
            return _dataToEntityMapper.Map(request);
        }

        public IEnumerable<Contract.Request> GetRequestsByUserid(int userId)
        {
            return _dbContext.Requests.Where(r => r.RequestorId == userId).Select(_dataToEntityMapper.Map);
        }

        public IEnumerable<Contract.Request> GetRequestsByAssistantid(int assistantId)
        {
            return _dbContext.Requests.Where(r => r.AssistantId == assistantId).Select(_dataToEntityMapper.Map);
        }

        public int AddRequest(Contract.Request request)
        {
            var dataRequest = _entityToDataMapper.Map(request);
            _dbContext.Requests.Add(dataRequest);
            return _dbContext.SaveChanges();
        }

        public int UpdateRequest(Contract.Request request)
        {
            var newData = _entityToDataMapper.Map(request);
            var oldData = _dbContext.Requests.Single(r=>r.Id==request.Id);
            _dbContext.Entry(oldData).CurrentValues.SetValues(newData);
            return _dbContext.SaveChanges();
        }
    }
}
