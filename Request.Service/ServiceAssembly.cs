﻿using System.Reflection;

namespace Request.Service
{
    public class ServiceAssembly
    {
        public static readonly Assembly Identity = typeof(ServiceAssembly).Assembly;
    }
}
