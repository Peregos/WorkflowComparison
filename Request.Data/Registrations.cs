﻿using Autofac;
using Request.Data.Context;

namespace Request.Data
{
    internal class Registrations : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<RequestDbContext>()
                .As<IRequestDbContext>();
        }
    }
}
