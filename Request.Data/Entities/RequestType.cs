﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Request.Data.Entities
{
    public class RequestType
    {
        [Key]
        public int Id { get; set; }
        [MaxLength(50)]
        [Required]
        public string Name { get; set; }

        public virtual ICollection<Request> Requests { get; set; }
    }
}
