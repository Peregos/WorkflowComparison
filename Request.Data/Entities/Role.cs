﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Request.Data.Entities
{
    public class Role
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }

        public virtual ICollection<UserGroup> UserGroups { get; set; }
    }
}
