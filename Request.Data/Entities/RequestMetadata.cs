﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Request.Data.Entities
{
    public class RequestMetadata
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public int RequestId { get; set; }
        [Required]
        public string Parameter { get; set; }
        [Required]
        public string Value { get; set; }

        [ForeignKey("RequestId")]
        public virtual Request Request { get; set; }
    }
}
