﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Request.Data.Entities
{
    public class User
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [MaxLength(100)]
        public string Name { get; set; }

        public virtual ICollection<UserGroup> UserGroups { get; set; }
    }
}
