﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Request.Data.Entities
{
    public class UserGroup
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }

        public virtual ICollection<User> GroupMembers { get; set; }
        public virtual ICollection<Role> Roles { get; set; }
    }
}
