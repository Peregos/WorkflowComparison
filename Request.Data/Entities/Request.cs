﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Request.Data.Entities
{
    public class Request
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public int TypeId { get; set; }
        [Required]
        public int RequestorId { get; set; }
        [Required]
        public int AssistantId { get; set; }
        [Required]
        public string Status { get; set; }
        [Required]
        public DateTime DateOfCreation { get; set; }

        [ForeignKey("RequestorId")]
        public virtual User Requestor { get; set; }
        [ForeignKey("AssistantId")]
        public virtual User Assistant { get; set; }
        [ForeignKey("TypeId")]
        public virtual RequestType Type { get; set; }
    }
}
