﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Request.Data.Entities
{
    public class RequestHistoryElement
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public int RequestId { get; set; }
        [Required]
        public int ActorId { get; set; }
        [Required]
        public DateTime DateOfModification { get; set; }
        [Required]
        public string Action { get; set; }

        [ForeignKey("ActorId")]
        public virtual User Actor{ get; set; }
        [ForeignKey("RequestId")]
        public virtual Request Request { get; set; }
    }
}
