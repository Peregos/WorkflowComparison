namespace Request.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initialization : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.RequestHistoryElements",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        RequestId = c.Int(nullable: false),
                        ActorId = c.Int(nullable: false),
                        DateOfModification = c.DateTime(nullable: false),
                        Action = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.ActorId)
                .ForeignKey("dbo.Requests", t => t.RequestId)
                .Index(t => t.RequestId)
                .Index(t => t.ActorId);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.UserGroups",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Roles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Requests",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TypeId = c.Int(nullable: false),
                        RequestorId = c.Int(nullable: false),
                        AssistantId = c.Int(nullable: false),
                        Status = c.String(nullable: false),
                        DateOfCreation = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.AssistantId)
                .ForeignKey("dbo.Users", t => t.RequestorId)
                .ForeignKey("dbo.RequestTypes", t => t.TypeId)
                .Index(t => t.TypeId)
                .Index(t => t.RequestorId)
                .Index(t => t.AssistantId);
            
            CreateTable(
                "dbo.RequestTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 50),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.RequestMetadatas",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        RequestId = c.Int(nullable: false),
                        Parameter = c.String(nullable: false),
                        Value = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Requests", t => t.RequestId)
                .Index(t => t.RequestId);
            
            CreateTable(
                "dbo.GroupUserMaps",
                c => new
                    {
                        GroupId = c.Int(nullable: false),
                        UserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.GroupId, t.UserId })
                .ForeignKey("dbo.UserGroups", t => t.GroupId)
                .ForeignKey("dbo.Users", t => t.UserId)
                .Index(t => t.GroupId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.GroupRoleMaps",
                c => new
                    {
                        GroupId = c.Int(nullable: false),
                        RoleId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.GroupId, t.RoleId })
                .ForeignKey("dbo.UserGroups", t => t.GroupId)
                .ForeignKey("dbo.Roles", t => t.RoleId)
                .Index(t => t.GroupId)
                .Index(t => t.RoleId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.RequestMetadatas", "RequestId", "dbo.Requests");
            DropForeignKey("dbo.RequestHistoryElements", "RequestId", "dbo.Requests");
            DropForeignKey("dbo.Requests", "TypeId", "dbo.RequestTypes");
            DropForeignKey("dbo.Requests", "RequestorId", "dbo.Users");
            DropForeignKey("dbo.Requests", "AssistantId", "dbo.Users");
            DropForeignKey("dbo.RequestHistoryElements", "ActorId", "dbo.Users");
            DropForeignKey("dbo.GroupRoleMaps", "RoleId", "dbo.Roles");
            DropForeignKey("dbo.GroupRoleMaps", "GroupId", "dbo.UserGroups");
            DropForeignKey("dbo.GroupUserMaps", "UserId", "dbo.Users");
            DropForeignKey("dbo.GroupUserMaps", "GroupId", "dbo.UserGroups");
            DropIndex("dbo.GroupRoleMaps", new[] { "RoleId" });
            DropIndex("dbo.GroupRoleMaps", new[] { "GroupId" });
            DropIndex("dbo.GroupUserMaps", new[] { "UserId" });
            DropIndex("dbo.GroupUserMaps", new[] { "GroupId" });
            DropIndex("dbo.RequestMetadatas", new[] { "RequestId" });
            DropIndex("dbo.Requests", new[] { "AssistantId" });
            DropIndex("dbo.Requests", new[] { "RequestorId" });
            DropIndex("dbo.Requests", new[] { "TypeId" });
            DropIndex("dbo.RequestHistoryElements", new[] { "ActorId" });
            DropIndex("dbo.RequestHistoryElements", new[] { "RequestId" });
            DropTable("dbo.GroupRoleMaps");
            DropTable("dbo.GroupUserMaps");
            DropTable("dbo.RequestMetadatas");
            DropTable("dbo.RequestTypes");
            DropTable("dbo.Requests");
            DropTable("dbo.Roles");
            DropTable("dbo.UserGroups");
            DropTable("dbo.Users");
            DropTable("dbo.RequestHistoryElements");
        }
    }
}
