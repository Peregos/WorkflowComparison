﻿using System.Reflection;

namespace Request.Data
{
    public class DataAssembly
    {
        public static readonly Assembly Identity = typeof(DataAssembly).Assembly;
    }
}
