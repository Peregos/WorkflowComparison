﻿using System.Data.Entity;
using Request.Data.Entities;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace Request.Data.Context
{
    public class RequestDbContext : DbContext, IRequestDbContext
    {

        public RequestDbContext() : base("EpamConnectionString")
		{
            Database.SetInitializer<RequestDbContext>(new DropCreateDatabaseIfModelChanges<RequestDbContext>());
            //Configuration.LazyLoadingEnabled = false;
        }

        public DbSet<RequestHistoryElement> RequestHistory{ get; set; }

        public DbSet<RequestMetadata> RequestMetadatas{ get; set; }

        public DbSet<Entities.Request> Requests { get; set; }

        public DbSet<Role> Roles{ get; set; }

        public DbSet<UserGroup> UserGroups{ get; set; }

        public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();

            modelBuilder.Entity<UserGroup>()
                .HasMany(g => g.Roles)
                .WithMany(r => r.UserGroups)
                .Map(m =>
                {
                    m.ToTable("GroupRoleMaps");
                    m.MapLeftKey("GroupId");
                    m.MapRightKey("RoleId");
                });

            modelBuilder.Entity<UserGroup>()
                .HasMany(g => g.GroupMembers)
                .WithMany(u => u.UserGroups)
                .Map(m =>
                {
                    m.ToTable("GroupUserMaps");
                    m.MapLeftKey("GroupId");
                    m.MapRightKey("UserId");
                });
        }

        public IDbSet<T> GetDbSet<T>() where T : class
        {
            return Set<T>();
        }
    }
}
