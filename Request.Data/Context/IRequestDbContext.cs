﻿using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using Request.Data.Entities;

namespace Request.Data.Context
{
    public interface IRequestDbContext
    {
        int SaveChanges();

        IDbSet<T> GetDbSet<T>() where T : class;

        DbEntityEntry<TEntity> Entry<TEntity>(TEntity entity) where TEntity : class;

        DbSet<RequestHistoryElement> RequestHistory { get; set; }

        DbSet<RequestMetadata> RequestMetadatas { get; set; }

        DbSet<Entities.Request> Requests { get; set; }

        DbSet<Role> Roles { get; set; }

        DbSet<UserGroup> UserGroups { get; set; }

        DbSet<User> Users { get; set; }
    }
}
