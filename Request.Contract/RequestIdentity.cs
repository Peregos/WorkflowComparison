﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace Request.Contract
{
    public class RequestIdentity : IIdentity
    {
        private const string DefaultAuthenticationType = "request";
        private readonly User _user;

        public RequestIdentity(User user)
        {
            _user = user;
        }

        public string Name => _user.Name;

        public string AuthenticationType => DefaultAuthenticationType;

        public bool IsAuthenticated => true;

        public User User => _user;
    }
}
