﻿namespace Request.Contract
{
    public enum RequestStatus
    {
        Draft = 1,
        Submitted = 2,
        Cancelled = 3,
        InProcess = 4,
        Done = 5
    }
}
