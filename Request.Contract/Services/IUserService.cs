﻿namespace Request.Contract.Services
{
    public interface IUserService
    {
        User GetUserById(int userId);

        int AddUser(User user);
    }
}
