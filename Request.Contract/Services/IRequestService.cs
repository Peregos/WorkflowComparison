﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Request.Contract.Services
{
    public interface IRequestService
    {
        Request GetRequestById(int requestId);

        IEnumerable<Request> GetRequestsByUserid(int userId);

        IEnumerable<Request> GetRequestsByAssistantid(int assistantId);

        int AddRequest(Request request);

        int UpdateRequest(Request request);
    }
}
