﻿namespace Request.Contract
{
    public enum Role
    {
        AuthorizedUser = 1,
        Assistant = 2,
        Administrator = 3
    }
}
