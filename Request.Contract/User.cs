﻿using System.Collections.Generic;

namespace Request.Contract
{
    public class User
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public ICollection<Role> Roles { get; set; }
    }
}
