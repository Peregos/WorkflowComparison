﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Request.Contract
{
    public class Request
    {
        public int Id { get; set; }
        public RequestType Type { get; set; }
        public DateTime DateOfCreation { get; set; }
        public RequestStatus Status { get; set; }
        
        public User Requestor { get; set; }
        public User Assistant { get; set; }
    }
}
